# scryfall-api
[![npm](https://img.shields.io/npm/v/scryfall-api.svg?style=flat-square)](https://www.npmjs.com/package/scryfall-api)
[![GitHub issues](https://img.shields.io/bitbucket/issues/MarioMH/scryfall-api.svg?style=flat-square)](https://bitbucket.org/MarioMH/scryfall-api.git)

A Node.js SDK for [Scryfall](https://scryfall.com/docs/api) written in Typescript.

As of [April 7th, 2021](./CHANGELOG.md), all features described in the [Scryfall documentation](https://scryfall.com/docs/api) are supported. If you see something that isn't supported, make an issue! See [support readme](./SUPPORT.md).


## Installation

```bat
npm install scryfall-api
```


## Documentation
Link to [documentation.](./DOCUMENTATION.md)


## Contributing

Thanks for wanting to help out! Here's the setup you'll have to do:
```bat
git clone https://bitbucket.org/MarioMH/scryfall-api.git
cd scryfall-api
npm install
```
You can now make changes to the repository. 

To compile, then test:
```bat
npm run build
```
To compile and then test on every file change:
```bat
npm run build:watch
```


## MIT License

[Copyright 2017-2021 Mackenzie McClane](./LICENSE)
