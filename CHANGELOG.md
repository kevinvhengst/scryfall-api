# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.7](https://bitbucket.org/MarioMH/scryfall-api/compare/v1.0.6...v1.0.7) (2021-06-16)

### [1.0.6](https://bitbucket.org/MarioMH/scryfall-api/compare/v1.0.5...v1.0.6) (2021-04-07)

### [1.0.5](http://bitbucket.org/MarioMH/scryfall-api/compare/v1.0.4...v1.0.5) (2021-04-07)

### [1.0.4](http://bitbucket.org/MarioMH/scryfall-api/compare/v1.0.3...v1.0.4) (2021-04-07)

### [1.0.3](http://bitbucket.org/MarioMH/scryfall-api/compare/v1.0.2...v1.0.3) (2021-04-07)

### [1.0.2](http://bitbucket.org/MarioMH/scryfall-api/compare/v1.0.1...v1.0.2) (2021-04-07)

### 1.0.1 (2021-04-07)
