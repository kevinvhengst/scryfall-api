import { EventEmitter } from 'eventemitter3';

export default class MagicEmitter<T, NOT_FOUND = never> extends EventEmitter {
  private _ended = false;
  public get ended(): boolean {
    return this._ended;
  }

  private _cancelled = false;
  public get cancelled(): boolean {
    return this._cancelled;
  }

  private _willCancelAfterPage = false;
  public get willCancelAfterPage(): boolean {
    return this._willCancelAfterPage;
  }

  public constructor() {
    super();
    this.on('end', () => {
      this._ended = true;
    });
    this.on('cancel', () => {
      this._ended = true;
    });
  }

  public on(event: 'data', listener: (data: T) => any): this;
  public on(event: 'not_found', listener: (data: NOT_FOUND) => any): this;
  public on(event: 'end', listener: (...args: any[]) => any): this;
  public on(event: 'cancel', listener: (...args: any[]) => any): this;
  public on(event: 'error', listener: (err: Error) => any): this;
  public on(event: 'done', listener: (...args: any[]) => any): this;
  public on(event: string, listener: (...args: any[]) => any): this {
    super.on(event, listener);
    return this;
  }

  public emit(event: 'data', data: T): boolean;
  public emit(event: 'not_found', data: NOT_FOUND): boolean;
  public emit(event: 'end'): boolean;
  public emit(event: 'cancel'): boolean;
  public emit(event: 'error', error: Error): boolean;
  public emit(event: 'done'): boolean;
  public emit(event: string, ...data: any[]): boolean {
    return super.emit(event, ...data);
  }

  public emitAll(event: 'data', ...data: T[]): void;
  public emitAll(event: 'not_found', ...data: NOT_FOUND[]): void;
  public emitAll(event: string, ...data: any[]): void {
    for (const item of data) {
      super.emit(event, item);
      if (this._cancelled) break;
    }
  }

  public cancel(): this {
    this._cancelled = true;
    this.emit('cancel');
    return this;
  }

  public cancelAfterPage(): this {
    this._willCancelAfterPage = true;
    return this;
  }

  public async waitForAll(): Promise<any> {
    type Result = T[] & { not_found: NOT_FOUND[] };
    return new Promise<Result>((resolve, reject) => {
      const results: Result = ([] as T[]) as Result;
      results.not_found = [];
      this.on('data', (result) => {
        results.push(result);
      });
      this.on('not_found', (notFound) => {
        results.not_found.push(notFound);
      });
      this.on('end', () => resolve(results));
      this.on('error', reject);
    });
  }

  public [Symbol.asyncIterator](): AsyncGenerator<T, void, unknown> {
    return this.generate('data');
  }

  public all(): AsyncGenerator<T, void, unknown> {
    return this.generate('data');
  }

  public notFound(): AsyncGenerator<NOT_FOUND, void, unknown> {
    return this.generate('not_found');
  }

  private generate(event: 'data'): AsyncGenerator<T, void, unknown>;
  private generate(
    event: 'not_found'
  ): AsyncGenerator<NOT_FOUND, void, unknown>;
  private async *generate(event: string) {
    // save the new data on each event
    const unYielded: any[] = [];
    this.on(event as never, (data) => unYielded.push(data));

    while (!this._ended) {
      // wait for the next event before yielding any new data
      await new Promise((resolve) => this.once(event as never, resolve));

      let data: T | NOT_FOUND | undefined;
      while ((data = unYielded.shift())) yield data;
    }
  }
}
