import commonjs from '@rollup/plugin-commonjs';
import json from '@rollup/plugin-json';
import node from '@rollup/plugin-node-resolve';
import sourcemaps from 'rollup-plugin-sourcemaps';
import { terser } from 'rollup-plugin-terser';
import typescript from 'rollup-plugin-typescript2';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const pkg = require('./package.json');
// eslint-disable-next-line @typescript-eslint/no-var-requires
const pkgs = require('./globals.json');

const umdLib = pkg.main;
const umdLibMin = umdLib.replace('.js', '.min.js');
const esLib = pkg.module;
const esLibMin = esLib.replace('.js', '.min.js');
const global = pkgs[pkg.name];
if (!global) {
  throw new Error(`Can not fin global package: '${pkg.name}'`);
}
const external = pkg.dependencies ? Object.keys(pkg.dependencies) : [];
const globals = {};
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
external.forEach((k) => (globals[k] = pkgs[k]));

export default {
  input: `src/index.ts`,
  output: [
    {
      format: 'umd',
      sourcemap: true,
      globals,
      file: `${umdLib}`,
      name: `${global}`,
    },
    {
      format: 'umd',
      sourcemap: true,
      globals,
      file: `${umdLibMin}`,
      name: `${global}`,
      plugins: [terser()],
    },
    { format: 'es', sourcemap: true, globals, file: `${esLib}` },
    {
      format: 'es',
      sourcemap: true,
      globals,
      file: `${esLibMin}`,
      plugins: [terser()],
    },
  ],
  external,
  watch: {
    include: 'src/**',
  },
  plugins: [json(), typescript(), commonjs(), node(), sourcemaps()],
};
